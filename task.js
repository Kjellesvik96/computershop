// DOM elements
const elPC = document.getElementById('computer');

const elLoanBtn = document.getElementById('loanB');
const elWorkBtn = document.getElementById('workB');
const elBankBtn = document.getElementById('bankB');
const elAddBtn = document.getElementById('btn-add-pc');
const elBuyBtn = document.getElementById('buyB');

const elBalance = document.getElementById('currentBalance');
const elPayBalance = document.getElementById('payValue');

const elSpec1 = document.getElementById('spec1');
const elSpec2 = document.getElementById('spec2');
const elSpec3 = document.getElementById('spec3');
const elPcPrice = document.getElementById('pcPrice');

const elInfo = document.getElementById('info');

const elSaleId = document.getElementById('saleId')
const elSaleDescription = document.getElementById('saleDescription')
const elSalePrice = document.getElementById('salePrice')
const elSaleImage = document.getElementById('picture');

let balance = 500;
let payBalance = 0;

//Button that lets you apply for a loan, may be accepted, or may not OK - disable knapp
elLoanBtn.addEventListener('click', function(event) {
    let loanPop = Number(window.prompt('How much do you want to loan?'));

    if (loanPop === 0) {
        elInfo.innerText = 'Cancel complete, you may apply for a new loan anytime';
    } else {
        if (loanPop > 2*balance) {
            elInfo.innerText = 'Sorry, your loan request has been denied, try a lower sum';              
        } else {
            balance += loanPop;
            elBalance.innerText = (balance + ' kr');
            elInfo.innerText = `Your loan request got approved! You now have ${balance} kr in your account`;
            elLoanBtn.disabled = true;
        }
    }
});

//Button that lets you earn money each time you press it OK
elWorkBtn.addEventListener('click', function(event) {
    payBalance += 100;
    elPayBalance.innerText = `${payBalance} kr`;
    elInfo.innerText = `You just upped your balance with 100kr Your total is now: ${payBalance} kr`;
});

//Button that lets you put your working money to you actual account OK
elBankBtn.addEventListener('click', function(event) {
    if (payBalance <= 0) {
        elInfo.innerText = 'You have nothing to transfer';
    } else {
        balance += payBalance;
        elInfo.innerText = `You just transferred: ${payBalance} kr to your account.\n 
                                    Your work account is now empty`;
        payBalance = 0;
        elPayBalance.innerText = `${payBalance} kr`;
        elBalance.innerText = `${balance} kr`;
    }
});

//Added feuture of the dropdown menu, to change the details about the computer when changing item
elPC.addEventListener('change', function(event){
    const [image, name, price, description, spec1, spec2, spec3] = extractItemValues(elPC.value);
    elSpec1.innerText = spec1;
    elSpec2.innerText = spec2;
    elSpec3.innerText = spec3;
    elSaleImage.src = image;
});

//Button that adds the chosen computer to the "payment section"
elAddBtn.addEventListener('click', function(event)  {
    const [value, name, price, description] = extractItemValues(elPC.value);
    elSaleId.innerText = name;
    elSaleDescription.innerText = description;
    elSalePrice.innerText = `${price} kr`;
    elInfo.innerText = (`You just added a ${name} to your shopping cart.\n
                    If you want another computer, just choose the right one and click "add to order"`);
});
    
//Button that makes you buy the computer if you have enough cash
elBuyBtn.addEventListener('click', function(event) {
    const [value, name, price, description] = extractItemValues(elPC.value);

    if (balance >= price) {
        balance -= price;
        elBalance.innerText = `${balance} kr`;
        elSaleId.innerText = '';
        elSaleDescription.innerText = '';
        elSalePrice.innerText = '';
        elSaleImage.innerText = '';
        elInfo.innerText = `You just bought a new ${name} Enjoy!`;
        elLoanBtn.disabled = false;
    } else {
        elInfo.innerText = `You do not have enough money yet, keep up the good work`;
    }
});

//Help-function
const extractItemValues = pcItem => pcItem.split(',');

/*
* Constructor to create objects
* @param name (String)  
* @param price (String)
* @param description (String)  
* @param specs (String)
*/
function PcItem(image, name, price, description, spec1, spec2, spec3) {
    this.image = image;
    this.name = name;
    this.price = parseInt(price);
    this.description = description;
    this.spec1 = spec1;
    this.spec2 = spec2;
    this.spec3 = spec3;
}