**Computer Store**

This is the first task of the Noroff JavaScript course.

In this assignment we were suppose to make a computer store where we could buy a computer.
There were some minimum requirements, but you were also allowed to make your own customizations.


- You should be able to get a loan, if some requirements are met

- You should be able to work, to ear money

- You should be able to transfer the money from your work-account to your bank account

- You should be able to choose different computers from a dropdown menu

- If you have enough money, you should be able to buy a computer

Deadline: 14/8-2020